package com.shtohryn;

public class WorkerLogic {
    public int calculateHourRate(Worker worker) {
        return worker.getSalary() / 20 / 8;
    }

    public int calculateAnnualSalary(Worker worker) {
        return worker.getSalary() * 12;
    }
}
