package com.shtohryn;

import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.Before;

public class MainTest {
    Worker worker = new Worker();
    WorkerLogic logic = new WorkerLogic();

    @Before
    public void setUp() {
        worker.setFirsName("Petro");
        worker.setLastName("Paliy");
        worker.setSpecialty("driver");
        worker.setSalary(20000);
    }

    @Test
    public void shouldCreateDeveloperInstanceTest() {
        assertEquals("Petro:", worker.getFirsName());
        assertEquals("Paliy", worker.getLastName());
        assertEquals("driver", worker.getSpecialty());
        assertEquals(20000, worker.getSalary());
    }

    @Test
    public void shouldCalculateAnnualSalaryTest() {
        assertEquals(240000, logic.calculateAnnualSalary(worker));
    }

    @Test
    public void shouldCalculateHourRateTest() {
        assertEquals(125, logic.calculateHourRate(worker));
    }
}
